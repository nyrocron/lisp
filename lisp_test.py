from lisp import Environment, std_def, std_lib

import unittest

class TestBasicOperators(unittest.TestCase):
    def setUp(self):
        self.env = Environment(std_def)

    def test_add(self):
        self.assertEqual(self.env.eval_str_unpack('(+ 1 2)'), 3)
        self.assertEqual(self.env.eval_str_unpack('(+ (+ 1 2) 3)'), 6)
        self.assertEqual(self.env.eval_str_unpack('(+ 0 1)'), 1)
        self.assertEqual(self.env.eval_str_unpack('(+ 0 -10)'), -10)

    def test_sub(self):
        self.assertEqual(self.env.eval_str_unpack('(- 10 0)'), 10)
        self.assertEqual(self.env.eval_str_unpack('(- 1 3)'), -2)
        self.assertEqual(self.env.eval_str_unpack('(- 10 5)'), 5)
        self.assertEqual(self.env.eval_str_unpack('(- 0 3)'), -3)
        self.assertEqual(self.env.eval_str_unpack('(- (- 10 3) 3)'), 4)

    def test_mul(self):
        self.assertEqual(self.env.eval_str_unpack('(* 1 3)'), 3)
        self.assertEqual(self.env.eval_str_unpack('(* 0 3)'), 0)
        self.assertEqual(self.env.eval_str_unpack('(* 7 3)'), 21)
        self.assertEqual(self.env.eval_str_unpack('(* 6 3)'), 18)
        self.assertEqual(self.env.eval_str_unpack('(* (* 4 2) 3)'), 24)

    def test_div(self):
        self.assertEqual(self.env.eval_str_unpack('(/ 9 3)'), 3)
        self.assertEqual(self.env.eval_str_unpack('(/ 21 3)'), 7)
        self.assertEqual(self.env.eval_str_unpack('(/ (* 3 7) 3)'), 7)
        self.assertAlmostEqual(self.env.eval_str_unpack('(/ 10 3)'), 10 / 3)
        self.assertAlmostEqual(self.env.eval_str_unpack('(/ 10 (/ 9 3))'), 10 / 3)

class TestBoolean(unittest.TestCase):
    def setUp(self):
        self.env = Environment(std_def)

    def test_and(self):
        self.assertTrue(self.env.eval_str_unpack('(and True True)'))
        self.assertFalse(self.env.eval_str_unpack('(and True False)'))
        self.assertFalse(self.env.eval_str_unpack('(and False True)'))
        self.assertFalse(self.env.eval_str_unpack('(and False False)'))

    def test_or(self):
        self.assertTrue(self.env.eval_str_unpack('(or True True)'))
        self.assertTrue(self.env.eval_str_unpack('(or True False)'))
        self.assertTrue(self.env.eval_str_unpack('(or False True)'))
        self.assertFalse(self.env.eval_str_unpack('(or False False)'))

    def test_not(self):
        self.assertTrue(self.env.eval_str_unpack('(not False)'))
        self.assertFalse(self.env.eval_str_unpack('(not True)'))

    def test_if(self):
        self.assertEqual(self.env.eval_str_unpack('(if True 1 0)'), 1)
        self.assertEqual(self.env.eval_str_unpack('(if False 1 0)'), 0)
        self.assertEqual(self.env.eval_str_unpack('(if (and True False) 1 0)'), 0)

class TestComparison(unittest.TestCase):
    def setUp(self):
        self.env = Environment(std_def)

    def test_eq(self):
        self.assertTrue(self.env.eval_str_unpack('(= 1 1)'))
        self.assertFalse(self.env.eval_str_unpack('(= 0 1)'))

class TestLambda(unittest.TestCase):
    def setUp(self):
        self.env = Environment(std_def)

    def test_simple_lambda(self):
        self.assertEqual(self.env.eval_str_unpack('((lambda (x) (+ x 1)) 2)'), 3)
        self.assertEqual(self.env.eval_str_unpack('((lambda (x y) (+ x y)) 2 3)'), 5)

class TestSet(unittest.TestCase):
    def setUp(self):
        self.env = Environment(std_def)

    def test_set_value(self):
        self.env.eval_str_unpack('(set x 5)')
        self.assertEqual(self.env.eval_str_unpack('x'), 5)

        self.env.eval_str_unpack('(set y -3)')
        self.assertEqual(self.env.eval_str_unpack('y'), -3)

        self.assertEqual(self.env.eval_str_unpack('(+ x y)'), 2)

    def test_set_function(self):
        self.env.eval_str_unpack('(set add (lambda (x y) (+ x y)))')
        self.assertEqual(self.env.eval_str_unpack('(add 2 3)'), 5)

        # factorial
        self.env.eval_str_unpack('(set fac (lambda (n) (if (= n 0) 1 (* n (fac (- n 1))))))')
        self.assertEqual(self.env.eval_str_unpack('(fac 0)'), 1)
        self.assertEqual(self.env.eval_str_unpack('(fac 1)'), 1)
        self.assertEqual(self.env.eval_str_unpack('(fac 2)'), 2)
        self.assertEqual(self.env.eval_str_unpack('(fac 3)'), 6)
        self.assertEqual(self.env.eval_str_unpack('(fac 4)'), 24)

class TestList(unittest.TestCase):
    def setUp(self):
        self.env = Environment(std_def, std_lib)

    def tearDown(self):
        del self.env

    def test_cons(self):
        self.assertEqual(self.env.eval_str('(cons 1 empty)'), ('lst', ('val', 1), ('lst',)))

    def test_head_tail(self):
        self.env.eval_str_unpack('(set l (cons 1 (cons 2 empty)))')

        self.assertEqual(self.env.eval_str_unpack('(head l)'), 1)
        self.assertEqual(self.env.eval_str_unpack('(head (tail l))'), 2)

    def test_list_equal(self):
        self.env.eval_str('(set a (cons 1 (cons 2 empty)))')
        self.env.eval_str('(set b (cons 1 (cons (+ 1 1) empty)))')
        self.assertTrue(self.env.eval_str_unpack('(~ a b)'))

        self.assertFalse(self.env.eval_str_unpack('(~ a (cons 1 a))'))


if __name__ == '__main__':
    unittest.main()
