from operator import add, sub, mul, truediv, and_, or_, not_, eq

def val_f(f):
    def inner(env, *args):
        arg_values = []
        for arg in args:
            evaluated_arg = env.eval(arg)
            assert(evaluated_arg[0] == 'val')
            arg_values.append(evaluated_arg[1])
        return ('val', f(*arg_values))
    return ('func', inner)

def if_f(env, cond_expr, true_expr, false_expr):
    t, v = env.eval(cond_expr)
    assert(t == 'val')
    return env.eval(true_expr) if v else env.eval(false_expr)

def lambda_f(env, l_ids_expr, l_expr):
    l_ids_t, l_ids_v = l_ids_expr[0], l_ids_expr[1:]
    assert(l_ids_t == 'expr')
    l_ids = []
    for ident in l_ids_v:
        id_t, id_v = ident
        assert(id_t == 'id')
        l_ids.append(id_v)

    def generated_function(inner_env, *inner_args):
        assert(len(l_ids) == len(inner_args))
        lambda_defs = dict()
        for i in range(len(l_ids)):
            lambda_defs[l_ids[i]] = inner_env.eval(inner_args[i])
        eval_env = Environment(inner_env.defs())
        eval_env.inner_defs = lambda_defs
        return eval_env.eval(l_expr)

    return ('func', generated_function)

def set_f(env, id_expr, val_expr):
    id_t, id_v = id_expr
    assert(id_t == 'id')
    env.inner_defs[id_v] = env.eval(val_expr)

def cons_f(env, head, tail):
    return ('lst', head, tail)

def head_f(env, lst):
    lst = env.eval(lst)
    assert(lst[0] == 'lst')
    if len(lst) == 1:
        raise Exception("head can only be called on non-empty lists")
    return env.eval(lst[1])

def tail_f(env, lst):
    lst = env.eval(lst)
    assert(lst[0] == 'lst')
    if len(lst) == 1:
        raise Exception("tail can only be called on non-empty lists")
    return env.eval(lst[2])

const_def = {
    'True': ('val', True),
    'False': ('val', False),
    'empty': ('lst',),
}

std_def = {
    '+': val_f(add),
    '-': val_f(sub),
    '*': val_f(mul),
    '/': val_f(truediv),
    'and': val_f(and_),
    'or': val_f(or_),
    'not': val_f(not_),
    '=': val_f(eq),
    'cons': ('func', cons_f),
    'head': ('func', head_f),
    'tail': ('func', tail_f),
    'empty?': ('func', lambda env, l: ('val', env.eval(l) == const_def['empty'])),
    'if': ('func', if_f),
    'lambda': ('func', lambda_f),
    'set': ('func', set_f),
}

std_lib = [
    # list comparison
    """
    (set ~ (lambda (x y)
        (if (empty? x)
            (if (empty? y)
                True
                False
            )
            (if (empty? y)
                False
                (and (= (head x) (head y))
                     (~ (tail x) (tail y))
                )
            )
        )
    ))
    """,
]

class Environment(object):
    def __init__(self, outer_defs, load=None):
        self.inner_defs = dict()
        self.outer_defs = outer_defs

        if load is not None:
            self.eval_list(load)

    def defs(self):
        return dictmerge(self.outer_defs, self.inner_defs)

    def eval_list(self, in_strs):
        for in_str in in_strs:
            self.eval_str(in_str)

    def eval_str(self, in_str):
        expr = self.parse(in_str)
        return self.eval(expr)

    def eval_str_unpack(self, in_str):
        expr = self.parse(in_str)
        result = self.eval(expr)

        if result is None:
            return 'None'

        if result[0] == 'val':
            return result[1]
        else:
            return result

    def parse(self, in_str):
        return self.read_expr(self.tokenize(in_str))

    def read_expr(self, tokens):
        token = tokens.pop(0)

        if token == ')':
            raise SyntaxError("Unexpected )")

        if token == '(':
            expr_content = []
            while tokens[0] != ')':
                expr_content.append(self.read_expr(tokens))
            tokens.pop(0)
            return ('expr',) + tuple(expr_content)

        if token in const_def:
            return const_def[token]

        try:
            return ('val', int(token))
        except ValueError:
            try:
                return ('val', float(token))
            except ValueError:
                return ('id', token)

    def tokenize(self, in_str):
        return in_str.replace('(', ' ( ').replace(')', ' ) ').split()

    def eval(self, expr):
        t = expr[0]

        if t in ['val', 'func', 'lst']:
            return expr

        if t == 'id':
            ident = expr[1]
            if ident in self.inner_defs:
                return self.inner_defs[ident]
            if ident in self.outer_defs:
                return self.outer_defs[ident]
            raise LookupError("Unknown identifier", ident)

        if t == 'expr':
            t, f = self.eval(expr[1])

            if t == 'func':
                return f(self, *expr[2:])

            raise Exception("Invalid expression, function expected")

def dictmerge(outer, inner):
    result = dict()
    for k in outer.keys():
        result[k] = outer[k]
    for k in inner.keys():
        result[k] = inner[k]
    return result

# basic REPL
if __name__ == '__main__':
    env = Environment(std_def, std_lib)

    while True:
        in_str = input('> ')
        if in_str in ['exit', 'quit']:
            break
        print(env.eval_str(in_str))
